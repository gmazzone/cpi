# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-24 18:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cpi', '0006_auto_20170824_1850'),
    ]

    operations = [
        migrations.CreateModel(
            name='Person_Concept',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('success_percent', models.DecimalField(decimal_places=2, max_digits=5)),
                ('update_date', models.DateTimeField()),
                ('concept_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cpi.Concept')),
                ('person_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cpi.Person')),
                ('sentiment_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cpi.Sentiment')),
            ],
        ),
        migrations.AddField(
            model_name='person',
            name='concepts',
            field=models.ManyToManyField(through='cpi.Person_Concept', to='cpi.Concept'),
        ),
    ]
