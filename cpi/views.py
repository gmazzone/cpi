# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Person

# Create your views here.
from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse


# Create your views here.

def index(request):
    pers = Person.objects.get(name_1 = 'Bernardo')
    context = {
        'pers': pers
    }
    template = loader.get_template('cpi/index.html')
    return HttpResponse(template.render(context, request))


def gentella_html(request):
    #context = {}
    pers = Person.objects.get(name_1='Bernardo')
    context = {
        'pers': pers
    }

    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.
    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('cpi/' + load_template)
    return HttpResponse(template.render(context, request))