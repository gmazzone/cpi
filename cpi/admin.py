# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Company, Person, Certification, Social_Network,Concept, Location, Contact_Type, Industry, Currency, \
    Attr_Table, Attr_Value_Type,Billing_Range, Certification_Company, \
    Certification_Person, Certification_Type, Company_Concept, Company_Fact_Monthly, \
    Company_Fact_Social_Network, Company_Fact_Yearly, Company_Office,Company_Office_Location,Company_Type,\
    Contact_Type_Company,Contact_Type_Person,Country,Ext_Attr_Values, Employees_Range,Extended_Attr_Table,\
    Industry_Company,Person_Concept,Person_Location,Position,Sentiment,Sex,Social_Network_Company,Social_Network_Person
from core.models import Fuente, Clasificacion_Fuente, Tipo_Fuente
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import GroupAdmin, UserAdmin

# New Admin Site template with dif texts
class CPIAdminSite(admin.AdminSite):
    site_header = 'CPI Backend Administration'
    site_title =  'CPI admin'
    site_url = 'http://localhost:8000/cpi/'
    index_title = 'CPI Administration Site'

# Registering the new admin site
admin_site = CPIAdminSite(name='cpiadmin')
admin_site.register(Company)
admin_site.register(Person)
admin_site.register(Certification)
admin_site.register(Certification_Type)
admin_site.register(Social_Network_Person)
admin_site.register(Social_Network)
admin_site.register(Concept)
admin_site.register(Location)
admin_site.register(Contact_Type_Company)
admin_site.register(Contact_Type)
admin_site.register(Contact_Type_Person)
admin_site.register(Industry)
admin_site.register(Currency)
admin_site.register(Attr_Value_Type)
admin_site.register(Attr_Table)
admin_site.register(Extended_Attr_Table)
admin_site.register(Ext_Attr_Values)
admin_site.register(Billing_Range)
admin_site.register(Social_Network_Company)
admin_site.register(Certification_Person)
admin_site.register(Certification_Company)
admin_site.register(Company_Concept)
admin_site.register(Company_Fact_Social_Network)
admin_site.register(Company_Fact_Yearly)
admin_site.register(Company_Fact_Monthly)
admin_site.register(Company_Office)
admin_site.register(Company_Office_Location)
admin_site.register(Company_Type)
admin_site.register(Country)
admin_site.register(Employees_Range)
admin_site.register(Industry_Company)
admin_site.register(Person_Concept)
admin_site.register(Person_Location)
admin_site.register(Position)
admin_site.register(Sentiment)
admin_site.register(Sex)

admin_site.register(Tipo_Fuente)
admin_site.register(Clasificacion_Fuente)
admin_site.register(Fuente)

# In order to maintain the user and group administration section.
admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)