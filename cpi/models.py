# -*- coding: utf-8 -*-
from django.db import models
DEFAULT_VALUE = -1
MAX_DIGITS = 5
DECIMAL_PLACES = 2

class Country(models.Model):
    country_id = models.AutoField(primary_key=True)
    country_code = models.CharField(max_length=50)
    country_name = models.CharField(max_length=100)
    country_continent = models.CharField(max_length=50)


class Sentiment(models.Model):
    sentiment_id = models.AutoField(primary_key=True)
    sentiment_desc = models.CharField(max_length=50)
    sentiment_explain = models.CharField(max_length=200)

    def __str__(self):
        return "%s" %(self.sentiment_desc)


class Certification_Type(models.Model):
    certification_type_id = models.AutoField(primary_key=True)
    certification_type_desc = models.CharField(max_length=50)
    update_date = models.DateTimeField()

    def __str__(self):
        return "%s" %self.certification_type_desc


class Certification(models.Model):
    certification_id = models.AutoField(primary_key=True)
    certification_code = models.CharField(max_length=50)
    certification_desc = models.CharField(max_length=200)
    certification_type_id = models.ForeignKey(Certification_Type)
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Certifications"

    def __str__(self):
        return "%s - %s" %(self.certification_code, self.certification_desc)

class Location(models.Model):
    location_id = models.AutoField(primary_key=True)
    state_code = models.CharField(max_length=20)
    state_name = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    x = models.CharField(max_length=50)
    y = models.CharField(max_length=50)
    country_id = models.ForeignKey(Country)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()

class Concept(models.Model):
    concept_id = models.AutoField(primary_key=True)
    concept_text = models.CharField(max_length=50)
    source = models.CharField(max_length=50)
    sentiment_id = models.ForeignKey(Sentiment)
    success_percent = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    update_date = models.DateTimeField()

    def __str__(self):
        return "%s - %s - %s" %(self.concept_text, self.source, self.success_percent)


class Social_Network(models.Model):
    social_network_id = models.AutoField(primary_key=True)
    social_network_name = models.CharField(max_length=50)
    social_network_desc = models.CharField(max_length=200)
    social_network_url = models.URLField()
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Social Networks"

    def __str__(self):
        return "%s" %(self.social_network_name)


class Sex(models.Model):
    sex_id = models.AutoField(primary_key=True)
    sex_code = models.CharField(max_length=20)
    sex_desc1 = models.CharField(max_length=50)
    sex_desc2 = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "Sex"

    def __str__(self):
        return "%s %s" % (self.sex_code, self.sex_desc1)



class Contact_Type(models.Model):
    contact_type_id = models.AutoField(primary_key=True)
    contact_type_desc = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "Types of Contacts"

    def __str__(self):
        return "%s" % self.contact_type_desc


class Person(models.Model):
    person_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    first_surname = models.CharField(max_length=50)
    last_surname = models.CharField(max_length=50)
    sex_id = models.ForeignKey(Sex)
    birth_date = models.DateField()
    born_country_id = models.ForeignKey(Country)
    update_date = models.DateTimeField()
    contact_types = models.ManyToManyField(Contact_Type, through='Contact_Type_Person')
    locations = models.ManyToManyField(Location, through='Person_Location')
    concepts = models.ManyToManyField(Concept, through='Person_Concept')
    social_networks = models.ManyToManyField(Social_Network, through='Social_Network_Person')
    certifications = models.ManyToManyField(Certification, through='Certification_Person')

    class Meta:
        verbose_name_plural = "People"

    def __str__(self):
        return "%s %s %s %s" %(self.first_name, self.second_name, self.first_surname, self.last_surname)


class Company_Type(models.Model):
    company_type_id = models.AutoField(primary_key=True)
    company_type_desc = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = "Types of Company"

    def __str__(self):
        return "%s" % self.company_type_desc


class Company(models.Model):
    company_id = models.AutoField(primary_key=True)
    company_code = models.CharField(max_length=50)
    company_name = models.CharField(max_length=100)
    company_brief = models.CharField(max_length=200)
    company_url = models.URLField()
    origin_country = models.ForeignKey(Country)
    establish_date = models.DateField()
    company_type_id = models.ForeignKey(Company_Type)
    active_flag = models.BooleanField(default=True)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()
    concepts = models.ManyToManyField(Concept, through='Company_Concept')
    certifications = models.ManyToManyField(Certification, through='Certification_Company')
    #clients = models.ManyToManyField(Company, through='Company_Client')

    class Meta:
        verbose_name_plural = "Companies"

    def __str__(self):
        return "%s" % self.company_name


class Extended_Attr_Table(models.Model):
    e_attr_id = models.AutoField(primary_key=True)
    table_name = models.CharField(max_length=50, blank=False)
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Extended Attribute Table"

    def __str__(self):
        return "%s" %self.table_name

class Attr_Value_Type(models.Model):
    attr_value_type_id = models.AutoField(primary_key=True)
    attr_value_type_desc = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "Attribute Value Type"

    def __str__(self):
        return "%s" %self.attr_value_type_desc


class Attr_Table(models.Model):
    attr_id = models.AutoField(primary_key=True)
    attr_name = models.CharField(max_length=50)
    attr_value_type_id = models.ForeignKey(Attr_Value_Type)
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Attribute Table"

    def __str__(self):
        return "%s" %self.attr_name


class Ext_Attr_Values(models.Model):
    e_attr_id = models.ForeignKey(Extended_Attr_Table)
    attr_id = models.ForeignKey(Attr_Table)
    attr_value = models.CharField(max_length=200)
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Extended Attribute Values"

    def __str__(self):
        return "%s - %s" %(self.attr_id, self.attr_value)

class Company_Office(models.Model):
    office_id = models.AutoField(primary_key=True)
    company_id = models.ForeignKey(Company)
    office_code = models.CharField(max_length=50)
    contact_types = models.ManyToManyField(Contact_Type, through='Contact_Type_Company')
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()
    locations = models.ManyToManyField(Location, through='Company_Office_Location')
    social_networks = models.ManyToManyField(Social_Network, through='Social_Network_Company')

class Contact_Type_Company(models.Model):
    contact_type_id = models.ForeignKey(Contact_Type, on_delete=models.CASCADE)
    #company_id = models.ForeignKey(Company_Office, on_delete=models.CASCADE)
    company_office_id = models.ForeignKey(Company_Office, on_delete=models.CASCADE)
    value = models.CharField(max_length=50)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()

    class Meta:
        verbose_name_plural = "Company Contact Type"

    #Me parece que el siguiente no va.
    #def __str__(self):
    #   return "%s" %self.table_name

class Industry(models.Model):
    industry_id = models.AutoField(primary_key=True)
    industry_code = models.CharField(max_length=20)
    industry_name = models.CharField(max_length=50)
    companies = models.ManyToManyField(Company, through='Industry_Company')

    class Meta:
        verbose_name_plural = "Industries"

    def __str__(self):
        return "%s - %s" %(self.industry_code, self.industry_name)


class Industry_Company(models.Model):
    industry_id = models.ForeignKey(Industry)
    company_id = models.ForeignKey(Company)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()

    def __str__(self):
        return "%s - %s" %(self.industry_id, self.company_id)

#class Company_Client(models.Model):
#    company_id = models.ForeignKey(Company)
#    client_id = models.ForeignKey(Company)
#    update_date = models.DateTimeField()
#    from_date = models.DateField()
#    to_date = models.DateField()
#
#    def __str__(self):
#       return "%s - %s" %(self.company_id, self.client_id)

class Contact_Type_Person(models.Model):
    contact_type_id = models.ForeignKey(Contact_Type)
    person_id = models.ForeignKey(Person)
    value = models.CharField(max_length=50)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()


class Position(models.Model):
    position_id = models.AutoField(primary_key=True)
    position_name = models.CharField(max_length=50)
    position_desc = models.CharField(max_length=50)
    update_date = models.DateTimeField()

    def __str__(self):
        return "%s - %s" %(self.position_name, self.position_desc)


#class Company_Person_Position(models.Model):
#    company_id = models.ForeignKey(Company_Office)
#    office_id = models.ForeignKey(Company_Office)
#    person_id = models.ForeignKey(Person)
#    position_id = models.ForeignKey(Position)
#    update_date = models.DateTimeField()
#    from_date = models.DateField()
#    to_date = models.DateField()

class Person_Location(models.Model):
    location_id = models.ForeignKey(Location)
    person_id = models.ForeignKey(Person)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()


class Company_Office_Location(models.Model):
    office_id = models.ForeignKey(Company_Office)
    location_id = models.ForeignKey(Location)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()


class Company_Concept(models.Model):
    company_id = models.ForeignKey(Company)
    concept_id = models.ForeignKey(Concept)
    sentiment_id = models.ForeignKey(Sentiment)
    success_percent = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    update_date = models.DateTimeField()

    def __str__(self):
        return "%s - %s" %(self.company_id, self.concept_id)


class Person_Concept(models.Model):
    person_id = models.ForeignKey(Person)
    concept_id = models.ForeignKey(Concept)
    sentiment_id = models.ForeignKey(Sentiment)
    success_percent = models.DecimalField(max_digits=MAX_DIGITS, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    update_date = models.DateTimeField()


class Social_Network_Person(models.Model):
    social_network_id = models.ForeignKey(Social_Network)
    person_id = models.ForeignKey(Person)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()

class Social_Network_Company(models.Model):
    social_network_id = models.ForeignKey(Social_Network)
    company_office_id = models.ForeignKey(Company_Office)
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()

class Currency(models.Model):
    currency_id = models.AutoField(primary_key=True)
    currency_code = models.CharField(max_length=20)
    currency_name = models.CharField(max_length=50)
    update_date = models.DateTimeField()

    class Meta:
        verbose_name_plural = "Currencies"

    def __str__(self):
        return "%s - %s" %(self.currency_code, self.currency_name)

class Billing_Range(models.Model):
    billing_range_id = models.AutoField(primary_key=True)
    billing_range_from = models.DecimalField(max_digits=15, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    billing_range_to = models.DecimalField(max_digits=15, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()


class Employees_Range(models.Model):
    employee_range_id = models.AutoField(primary_key=True)
    employee_range_from = models.IntegerField()
    employee_range_to = models.IntegerField()
    update_date = models.DateTimeField()
    from_date = models.DateField()
    to_date = models.DateField()



class Certification_Person(models.Model):
    certification_id = models.ForeignKey(Certification)
    person_id = models.ForeignKey(Person)
    certification_from_date = models.DateField()
    certification_to_date = models.DateField()
    update_date = models.DateTimeField()


class Certification_Company(models.Model):
    certification_id = models.ForeignKey(Certification)
    company_id = models.ForeignKey(Company)
    certification_from_date = models.DateField()
    certification_to_date = models.DateField()
    update_date = models.DateTimeField()

class Company_Fact_Monthly(models.Model):
    fact_date = models.DateField()
    company_office_id = models.ForeignKey(Company_Office)
    currency_id = models.ForeignKey(Currency)
    billing = models.DecimalField(max_digits=15, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    billing_range_id = models.ForeignKey(Billing_Range)
    employee_qty = models.IntegerField()
    employee_range_id = models.ForeignKey(Employees_Range)
    employee_qty_female = models.IntegerField()
    employee_qty_male = models.IntegerField()
    client_qty = models.IntegerField()
    tenders_postulate_qty = models.IntegerField()
    tenders_qty_won = models.IntegerField()


class Company_Fact_Yearly(models.Model):
    fact_date = models.DateField()
    company_office_id = models.ForeignKey(Company_Office)
    currency_id = models.ForeignKey(Currency)
    billing = models.DecimalField(max_digits=15, decimal_places=DECIMAL_PLACES) #Faltan decimales, etc
    billing_range_id = models.ForeignKey(Billing_Range)
    employee_qty = models.IntegerField()
    employee_range_id = models.ForeignKey(Employees_Range)
    employee_qty_female = models.IntegerField()
    employee_qty_male = models.IntegerField()
    client_qty = models.IntegerField()
    tenders_postulate_qty = models.IntegerField()
    tenders_qty_won = models.IntegerField()


class Company_Fact_Social_Network(models.Model):
    fact_date = models.DateField()
    company_office_id = models.ForeignKey(Company_Office)
    social_network_id = models.ForeignKey(Social_Network)
    activity = models.IntegerField()