# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

#Tipo fuente representa si es Twitter, un Canal RSS, BD, Archivo CSV, Archivo Excel, etc)
class Tipo_Fuente(models.Model):
    id_tipo_fuente = models.AutoField(primary_key=True)
    desc_tipo_fuente = models.CharField(max_length=100, blank=False)

    class Meta:
        verbose_name_plural = "Tipo Fuentes"

    def __str__(self):
        return "%s" % (self.desc_tipo_fuente)

#Clasificacion Fuente representa si es persona, empresa, etc la entidad
class Clasificacion_Fuente(models.Model):
    id_clasificacion_fuente = models.AutoField(primary_key=True)
    desc_clasificacion_fuente = models.CharField(max_length=100, blank=False)

    class Meta:
        verbose_name_plural = "Clasificaciones"

    def __str__(self):
        return "%s" % (self.desc_clasificacion_fuente)


#La fuente que se incorpora
class Fuente(models.Model):
    id_fuente = models.AutoField(primary_key=True)
    desc_fuente = models.CharField(max_length=100, blank=False)
    tipo_fuente = models.ForeignKey(Tipo_Fuente)
    clasificacion_fuente = models.ForeignKey(Clasificacion_Fuente)
    #Redes sociales de la fuente
    twitter_fuente = models.CharField(max_length=20,blank=True)
    linkedin_fuente = models.CharField(max_length=20,blank=True)
    url_fuente = models.CharField(max_length=20,blank=True)

    class Meta:
        verbose_name_plural = "Fuentes"

    def __str__(self):
        return "%s" %(self.desc_fuente)
